#!/usr/bin/env bash

SCRIPTNAME="mongodb"
BRANCH="latest"

if [ "$1" = "push" ];then 
  PUSH=1
else
  PUSH=0
fi

echo "building jbanetwork/${SCRIPTNAME}:${BRANCH}...";
docker build -t jbanetwork/${SCRIPTNAME}:${BRANCH} .;
if [[ $? == 0 ]];then
	if [[ $PUSH == 1 ]];then
		echo "pushing jbanetwork/${SCRIPTNAME}:${BRANCH}...";
		docker push jbanetwork/${SCRIPTNAME}:${BRANCH};
		if [[ $? != 0 ]];then
			exit $?;
		fi
	fi
else
    exit $?;
fi