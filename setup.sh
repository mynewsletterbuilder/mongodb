#!/usr/bin/env bash

mkdir /var/lib/mongodb;
chmod -R 777 /var/lib/mongodb;

mongod -f /etc/mongod.conf &

sleep 1
UP=0;
while [ ${UP} == 0 ]; do
	mongo --eval "rs.initiate()"
	if [ $? == 0 ];then
		UP=1;
	else
		printf "waiting on mongo...\r"
		sleep 1;
	fi
done

tail -F /var/log/mongodb/mongodb.log